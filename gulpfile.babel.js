/**
 * Automated tasks for website build and tests.
 *
 * @package yp-agreement-checkboxes
 */

const gulp = require( 'gulp' ),
	  sass = require('gulp-sass'),
	  postcss = require( 'gulp-postcss' ),
	  autoprefixer = require( 'autoprefixer' ),
	  browserSync = require('browser-sync').create(),
	  concat = require('gulp-concat'),
	  f_input = './src/scss/front/**/*.scss',
	  b_input = './src/scss/back/**/*.scss',
	  output = './assets/style';

/**
 * Set up browsersync.
 */
gulp.task('browserSync', function () {
	browserSync.init({
	  server: {
	  	baseDir: 'src'
	  },
	})
})

/**
 * Compile styles - backend.
 */
gulp.task(
	'style_back',
	() => {
		return gulp.src( b_input )
			.pipe( sass() )
			.pipe( postcss( [ autoprefixer() ] ) )
			.pipe( concat( 'yp_chbx_admin.css') )
			.pipe( gulp.dest( output ) )
			.pipe( browserSync.reload({
				stream: true
				} )
			)
	}
);

/**
 * Compile styles - frontend.
 */
gulp.task(
	'style_front',
	() => {
		return gulp.src( f_input )
			.pipe( sass() )
			.pipe( concat( 'yp_chbx_front.css' ) )
			.pipe( gulp.dest( output ) )
			.pipe( browserSync.reload({
				stream: true
				} )
			)
	}
);

/**
 * Build css and/or other auto-generated files
 */
gulp.task(
	'build',
	gulp.series(
		'style_back',
		'style_front'
	)
);
