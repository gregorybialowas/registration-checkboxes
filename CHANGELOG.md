# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Releases

### [1.0.2] - 2023-05-17

- Tested: WordPress 6.2.1, PHP 7.4

### [1.0.2] - 2020-02-26

#### Changed

- version changed to 1.0.2

#### Fixed

- script doesn't validate checkboxes if the Show on Front is set to 0
- few minor changes

### 1.0.1 - 2020-02-24

#### Fixed

- PHPCS sanitaztion issues

### 1.0.0 - 2019-12-24

#### Added

- Initial release

[1.0.2]: https://bitbucket.org/gregorybialowas/registration-checkboxes/src/master/
