Add custom agreement checkboxes to the registration form of your WooCommerce shop. The plugin also features a double password validation.

## WooCommerce Agreement Checkboxes and/or RODO Agreements

This plugin allows you to add custom agreement selectable checkboxes to the Registration Form of your online store. You can define which checkboxes are mandatory to be checked/selected in order to register a new account. The plugin features also a double password validation, which adds an extra field that prompts an User to repeat his/her password.

Already registered users can see the form right underneath their "account details" and the plugin forces them to check the mandatory checkboxes as well upon saving their details.

! IMPORTANT: in order to use this plugin, you must enable "Allow customers to create an account on the "My account" page"
in WooCommerce > Settings > Accounts & Privacy

# Usage

Download the package, istall and activate it. The widget will available under **WooCommerce** -> **Settings**
You will find two new tabs there: "**Agreement checkboxes**" and "**Double password**"

`In the Plugins section, activate Registration Checkboxes`

![](https://bitbucket.org/gregorybialowas/registration-checkboxes/raw/ccb449b2b4f768be97767994f6b979fd18457d83/assets/img/assets/screenshot-1.jpg)

`In the WooCommerce settings you'll see two tabs "Agreement checkboxes" and "Double password"`

![](https://bitbucket.org/gregorybialowas/registration-checkboxes/raw/ccb449b2b4f768be97767994f6b979fd18457d83/assets/img/assets/screenshot-2.jpg)

`Agreement checkboxes - Add / edit checkboxes by following the pattern`

![](https://bitbucket.org/gregorybialowas/registration-checkboxes/raw/ccb449b2b4f768be97767994f6b979fd18457d83/assets/img/assets/screenshot-3.jpg)

`Agreement checkboxes - Logged in user's settings`

![](https://bitbucket.org/gregorybialowas/registration-checkboxes/raw/ccb449b2b4f768be97767994f6b979fd18457d83/assets/img/assets/screenshot-4.jpg)

`Agreement checkboxes - New user registration`

![](https://bitbucket.org/gregorybialowas/registration-checkboxes/raw/ccb449b2b4f768be97767994f6b979fd18457d83/assets/img/assets/screenshot-5.jpg)

## WooCommerce Double Password feature

`Double password feature`

![](https://bitbucket.org/gregorybialowas/registration-checkboxes/raw/ccb449b2b4f768be97767994f6b979fd18457d83/assets/img/assets/screenshot-6.jpg)

`The plugin prompts a user to disable Woo's auto password generator (if enabled)`

![](https://bitbucket.org/gregorybialowas/registration-checkboxes/raw/ccb449b2b4f768be97767994f6b979fd18457d83/assets/img/assets/screenshot-7.jpg)

`Double password feature is set and is active`

![](https://bitbucket.org/gregorybialowas/registration-checkboxes/raw/ccb449b2b4f768be97767994f6b979fd18457d83/assets/img/assets/screenshot-8.jpg)

`Agreement checkboxes and Double password both in place - New user registration`

![](https://bitbucket.org/gregorybialowas/registration-checkboxes/raw/ccb449b2b4f768be97767994f6b979fd18457d83/assets/img/assets/screenshot-9.jpg)

# Where to use

WooCommerce Registration Checkboxes is dedicated to extend WooCommerce functionality so it requires WC to be installed and activated.
