module.exports = {
	"extends": [ "plugin:@wordpress/eslint-plugin/recommended" ],
	"parserOptions": {
		"ecmaVersion": 9,
		"sourceType": "module",
		"ecmaFeatures": {
			"modules": true
		}
	},
    "globals": {
		"jQuery": true,
		"parajax": true
    },
	"rules": {
		"padded-blocks": 0,
	}
};
